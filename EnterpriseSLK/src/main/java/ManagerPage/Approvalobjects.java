package ManagerPage;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Approvalobjects {
	
	//Approval
	
		@FindBy(xpath="//div[@class='tile-four']")
		public WebElement ApprovalDashboard;
		
		@FindBy(xpath="//div[@id='13']") 
        public WebElement ApprovalDashboard1;
		
		@FindBy(xpath="//li[@id='li_69']")
		public WebElement AwaitingforApproval;
		
		@FindBy(xpath="//div[@class='panel-body']/div/div/div/div[@id='s2id_status']")
		public WebElement Approvestatus;
		
		@FindBy(xpath="//div[contains(@id,'select2-drop')]/div/input")
	    public WebElement statusdropdown;
		
		@FindBy(xpath="//textarea[@id='remarks']")
        public WebElement Remarks;
		
		@FindBy(xpath="//button[@id='btnsave']")
		public WebElement submit;
		
		@FindBy(xpath="//button[@id='ApprBtn']")
		public WebElement caseapproved;
		
		@FindBy(xpath="//i[@class='fa fa-angle-up panel-tool-i iconCollapse fa-rotate-180']")
		public WebElement changeapproverexpand;
}
