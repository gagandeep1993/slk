package BeneficiaryPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ApplyforVisaobjects {

	@FindBy(xpath = "//div[@class='dashboard-layout']/a/div/div[@id='15']")
	public WebElement DashboardApplyforvisa;
	
	@FindBy(xpath = "//span[contains(text(),'My Case(s)')]")
	public WebElement Mycases;
	

	// Residing country
	@FindBy(xpath = "//div[@id='s2id_ddlSource']/a")
	public WebElement residingCountry;

	@FindBy(xpath = "//div[contains(@id,'select2-drop')]/div/input")
	public WebElement residingCountrysearch;

	// Destination country

	@FindBy(id = "txtServiceTypeSearch")
	public WebElement DestinationCountry;

	// travel purpose

	@FindBy(xpath = "//*[@id='s2id_ddlTravelPrupose']/ul")
	public WebElement TravelPurpose;

	@FindBy(xpath = "//div[@id='s2id_ddlTravelPrupose']/ul/li/input")
	public WebElement TravelPurposeSearch;

	@FindBy(id = "txtTravelDescription")
	public WebElement Description;

	// Passport

	@FindBy(xpath = "//div[@id='s2id_ddlPassportNo']")
	public WebElement passport;

	@FindBy(xpath = "//div[@id='select2-drop']/div/input")
	public WebElement passportsearch;

	// Initiate button
	@FindBy(id = "btnInitiate")
	public WebElement Initiate;

	// Case Initiation fields

	@FindBy(xpath = "//button[@id='btnRuleCheckSubmitCase']")
	public WebElement submit;

	// ------------Summary Page-----------------------//

	@FindBy(id = "//li[contains(text(),'Document')]")
	private WebElement Documents;

	public WebElement getDocuments() {
		return Documents;
	}

	// Questionnaire
	@FindBy(xpath = "//div[@class='responsive-tabs responsive-tabs--enabled']/ul/li[2]")
	public WebElement Questinnaire;

	@FindBy(xpath = "//button[@class='btnf btnf-success dlgConfirm modal-btn']")
	public WebElement Questionnairedialgbox;
	
	@FindBy(xpath="/html/body/div[6]/div/div/div[3]/button[1]")
	public WebElement Questionnairedialogbox;

	@FindBy(xpath = "//*[@id='quesCompleted']")
	public WebElement Questinnairecompleted;

	// Appoinment Questionnaire
	@FindBy(xpath = "//div[@class='responsive-tabs responsive-tabs--enabled']/ul/li[3]")
	public WebElement AppoinmentQuestinnaire;

	@FindBy(xpath = "//li[@id='tablist1-tab3']")
	public WebElement formmodule;
	
	@FindBy(xpath="//button[@class='btnf btnf-success dlgConfirm modal-btn']")
	public WebElement Formsdialogbox;
	
	// form
	
	@FindBy(xpath = "//a[@id='appquesCompleted']")
	public WebElement AppoinmentQuestionnairecompleted;
	
	@FindBy(xpath="//a[@id='formCompleted']")
    public WebElement formcompleted;
	
	// Document upload
	@FindBy(xpath = "//div[@class='responsive-tabs responsive-tabs--enabled']/ul/li[4]")
	public WebElement Documenttab;

	@FindBy(xpath="//li[@id='tablist1-tab3']")
	public WebElement Documenttab1;
	
	@FindBy(xpath = "//*[contains(text(),'Document Uploaded')]")
	public WebElement DocumentUploaded;

	@FindBy(xpath = "//*[contains(text(),'Submit Case')]")
	public WebElement SubmitCase;

	//logout
	
	@FindBy(xpath="//div[@class='nav-profile']")
	public WebElement logouticon;
	
	@FindBy(xpath="//button[@class='btn-logout']")
	public WebElement logoutbutton;
	
	//Dashboard
	@FindBy(xpath="//i[@class='fa fa-edit fa-5x']")
	public WebElement Myrecords;
	
	@FindBy(xpath="//h3[@class='mytasks-hthree']")
	public WebElement DashboardMytasks;
	
	
	
	
}



