package ScreeningPage;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class OrganizationPage {
	

	@FindBy(xpath = "//div[text()='Organization']")
	public WebElement OrganizationModule;

	@FindBy(xpath="//span[contains(text(), 'Client')]")
	public WebElement clientsubmodule;
	
	@FindBy(xpath="//a[@class='list-items-c Category']")
	public WebElement Addicon;
	
	@FindBy(xpath="//a[@class='btn btn-default btn-xs pull-right ViewBenificaiary']")
	public WebElement Plus;
	
	@FindBy(xpath="//div[@id='s2id_ddl_SerachCriteria']")
	public WebElement Searchcriteria;
	
	@FindBy(xpath="//input[@id='s2id_autogen8_search']")
	public WebElement Searchdropdown;
	
	@FindBy(xpath="//span[@id='select2-chosen-2']")
	public WebElement Selectcondition;
	
	@FindBy(xpath="//input[@id='s2id_autogen2_search']")
	public WebElement Selectdropdown;
	
	@FindBy(xpath="//input[@id='txtSearch1']")
    public WebElement Textbox;
	
	@FindBy(xpath="//button[@id='preview-records']")
	public WebElement searchbutton;
	
	@FindBy(xpath="//*[@id='tblClientList']/tbody/tr/td[1]/a")
	public WebElement BeneficiaryEmailid;
	
	@FindBy(xpath="//strong[text()='Add New Case']")
	public WebElement Addnewcase;

}
