package ScreeningPage;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AdvanceSearchPage {
	
	@FindBy(xpath="//div[contains(text(),'Screening')]")
	public WebElement screeningmodule;
	
	
	@FindBy(xpath="//*[contains(text(),'Advance Search')]")
	public WebElement advancesearch;
	
	
	//-----------screening search filters---------------------------//
	
	//Expand button
	
	@FindBy(xpath="//div[@class='fa fa-chevron-down pull-right rotate2 collapse-icon-padding']")
	public WebElement countrySearchExpand;
	
	@FindBy(xpath="//*[@id='s2id_SourceCountry']/a")
	public WebElement SourceCountry;
	
	
	@FindBy(xpath="//div[@id='select2-drop']/div/input")
	public WebElement SourceCountrySearch;
	
	
	@FindBy(xpath="//*[@id='s2id_DestinationCountry']/a")
	public WebElement DestinationCountry;
	
	@FindBy(xpath="//div[@id='select2-drop']/div/input")
	public WebElement DestinationCountrySearch;
	
	@FindBy(xpath="//*[@id='s2id_Service']/a")
	public WebElement service;
	
	
	@FindBy(xpath="//div[@id='select2-drop']/div/input")
	public WebElement serviceSearch;

	
	
	@FindBy(xpath="//*[@id='s2id_Type']/a")
	public WebElement type;
	

	@FindBy(xpath="//div[@id='select2-drop']/div/input")
	public WebElement typeSearch;

	
	
	@FindBy(xpath="//button[@id='Search']")
	public WebElement SearchButton;
	
	//---------------------------Case expand xpath---------------------//
	
	//Case expand screening button
	@FindBy(xpath="//a[contains(text(),'Screening')]")
	private WebElement Screeningexpand;
	public WebElement getScreeningButton()
	{
		return Screeningexpand;
	}
	
	
	//-------Questionnaire--------------//
	
	@FindBy(xpath="//div[contains(text(),'Questionnaire')]")
	public WebElement QuestionnaireModule;	
	
	@FindBy(xpath="//button[@class='btnf btnf-success dlgConfirm ']")
	public WebElement Questdialogbox;
	
	//form
	
	@FindBy(xpath="//div[@id='Forms']")
	public WebElement FormModule;
	
	@FindBy(xpath="//a[@id='formCompleted']")
	public WebElement formverified;
	
	@FindBy(xpath="//button[@class='btnf btnf-success dlgConfirm']")
	public WebElement Formdialogbox;
	//---------------Case Information--------------------------//
	
	//Case information Module
	@FindBy(id="CaseInformationclick")
	private WebElement CaseInformationmodule;
	public WebElement getCaseInformationModule()
	{
		return CaseInformationmodule;
	}
	
	//Section 1 click
	@FindBy(id="li1")
	private WebElement Section1;
	public WebElement getSection1()
	{
		return Section1;
	}
	
	//Section 2 click
	@FindBy(id="li2")
	private WebElement Section2;
	public WebElement getSection2()
	{
		return Section2;
	}
		
	//-------------------Document Module---------------------//
	@FindBy(xpath="//div[contains(text(),'Documents')]")
	public WebElement DocumentModule;
	
	
	//Document upload button
	
	@FindBy(xpath="//button[contains(@data-cur-doc,'1')]")
	private WebElement DocumentUploadButton1;
	public WebElement getDocumentUploadButton1()
	{
		return DocumentUploadButton1;
	}
	
	@FindBy(xpath="//button[contains(@data-cur-doc,'2')]")
	private WebElement DocumentUploadButton2;
	public WebElement getDocumentUploadButton2()
	{
		return DocumentUploadButton2;
	}
	
	@FindBy(xpath="//button[contains(@data-cur-doc,'3')]")
	private WebElement DocumentUploadButton3;
	public WebElement getDocumentUploadButton3()
	{
		return DocumentUploadButton3;
	}
	
	
	@FindBy(xpath="//a[@id='docUploadCompleted']")
	public WebElement DocumentVerification;
	
	//----------------Case Decision--------------//
	
	
	@FindBy(xpath="//div[@id='CaseVerification']")
	public WebElement CaseDecision;
	
	
	
	@FindBy(xpath="//div[@id='s2id_ddlCaseApprover']")
	public WebElement CaseVerfiSelectAction;
	
	
	
	@FindBy(xpath="//div[contains(@id,'select2-drop')]/div/input")
	public WebElement CaseVerfiSelectActionSearch;
	
	
	
	@FindBy(xpath="//input[@name='txtRemarks']")
	public WebElement CaseVerifRemark;
	
	
	@FindBy(xpath="//button[@id='btnSave']")
	public WebElement CaseVerifSave;
	
	//----Appoinment Module---------//
	
	@FindBy(xpath="//div[contains (text(),'Appointment Management')]")
	public WebElement Appoinmentmodule;
	
	@FindBy(xpath="//a[@id='quesappCompleted']")
    public WebElement ApponmentQuestionnaireverified;	
	
	//-------Progress Tracker----//
	
	@FindBy(xpath="//div[@id='ProgressTrackerclick']")
	public WebElement progressTrackerModule;
	
	@FindBy(xpath="//a[@id='sd_123']")
	public WebElement ProgressTrackexpand;
	
	@FindBy(xpath="//input[@id='date-label-w_1016']")
	public WebElement Trackerfield;
	
	@FindBy(xpath="//button[@class='btn btn-small btn-actions btn-save-cust triggerAction']")
	public WebElement savebtn;
	
	@FindBy(xpath="//input[@id='date-label-w_1017']")
	public WebElement Trackerfield2;
	
	//screening PR expand
	
	@FindBy(xpath="//a[@id='sd_39']")
	public WebElement screeningprexpand;
	 
	//Appoinment approval copy appload tracker
	
	@FindBy(xpath="//input[@id='date-label-w_1074']")
	public WebElement Trackerfield3;
	
	
	
	
	@FindBy(xpath="//a[@id='sd_259']")
	public WebElement Approvalcopyuploadexpand;
	
	
	
}



