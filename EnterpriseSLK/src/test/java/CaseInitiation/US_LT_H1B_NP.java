package CaseInitiation;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import BeneficiaryPage.ApplyforVisaobjects;
import ScreeningPage.AdvanceSearchPage;
import ScreeningPage.OrganizationPage;
import login.Loginobjects;


public class US_LT_H1B_NP {
	public WebDriver driver;
	String x;
	Loginobjects Lobjects = new Loginobjects();
	ApplyforVisaobjects Beneficiaryobjects = new ApplyforVisaobjects();
	OrganizationPage Orgobjects= new OrganizationPage();
	 AdvanceSearchPage Advancesearchobj=new AdvanceSearchPage();
	 
	@BeforeTest
	public void InitiateBrowser() {
		System.setProperty("webdriver.gecko.driver", "C:\\Enterprisefile\\geckodriver-v0.19.0-win64.exe");
		driver = new FirefoxDriver();
	}

	@Test(priority = 1)
	public void login() throws InterruptedException {

		PageFactory.initElements(driver, Lobjects);
		driver.get("http://10.10.10.101/enterpriseqa/");
		Lobjects.Username.sendKeys("screen@mailinator.com");
		Lobjects.Password.sendKeys("Password@1");
		Lobjects.signin.click();
		Thread.sleep(2000);
	}

@Test(priority = 2)

	public void Initiatecase() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(600, TimeUnit.SECONDS);
		PageFactory.initElements(driver, Beneficiaryobjects);
		PageFactory.initElements(driver, Orgobjects);
		Orgobjects.OrganizationModule.click();
		//Thread.sleep(4000);
		//Orgobjects.clientsubmodule.click();
		Thread.sleep(3000);
		/*Orgobjects.Addicon.click();
		Thread.sleep(2000);
		Orgobjects.Plus.click();
		Thread.sleep(3000);*/
		Orgobjects.Searchcriteria.click();
		Thread.sleep(3000);
		Orgobjects.Searchdropdown.sendKeys("beneficiary Email");
		Thread.sleep(3000);
		Orgobjects.Searchdropdown.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
		Thread.sleep(800);
		Orgobjects.Selectcondition.click();
		Orgobjects.Selectdropdown.sendKeys("Equals");
		Thread.sleep(2000);
		Orgobjects.Selectdropdown.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
		Thread.sleep(2000);
		Orgobjects.Textbox.sendKeys("bsix@mailinator.com");
		Thread.sleep(2000);
		Orgobjects.searchbutton.click();
		Thread.sleep(700);
		Orgobjects.BeneficiaryEmailid.click();
		Thread.sleep(2000);
		Orgobjects.Addnewcase.click();
		Beneficiaryobjects.residingCountry.click();
		Thread.sleep(2000);
		Beneficiaryobjects.residingCountrysearch.sendKeys("India");
		Thread.sleep(2000);
		Beneficiaryobjects.residingCountrysearch.sendKeys(Keys.ARROW_DOWN, Keys.ENTER,Keys.ARROW_DOWN, Keys.ENTER);
		Beneficiaryobjects.DestinationCountry.click();
		Thread.sleep(2000);
		Beneficiaryobjects.DestinationCountry.sendKeys("united states");
		Thread.sleep(3000);
		Beneficiaryobjects.DestinationCountry.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
		Thread.sleep(3000);
		//Beneficiaryobjects.TravelPurpose.click();
		//Beneficiaryobjects.TravelPurposeSearch.sendKeys("classroom Training");
		//Beneficiaryobjects.TravelPurposeSearch.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
		//Beneficiaryobjects.Description.sendKeys("Classroom Training");
		Beneficiaryobjects.passport.click();
		Beneficiaryobjects.passportsearch.sendKeys("IPF2343242");
		Beneficiaryobjects.passportsearch.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
		Thread.sleep(2000);
		Beneficiaryobjects.Initiate.click();
		Thread.sleep(6000);
		
		x = driver.findElement(By.xpath("//p[@id='pCIID']")).getText();
		System.out.println("The case no is" + x);
		Thread.sleep(15000);
		// scroll page down till Address1 button for firefox.
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("scroll(0,350)");
		
				Thread.sleep(2000);
		
		//driver.findElement(By.xpath("//input[contains(@label,'Address 2')]")).sendKeys("Jammu");
		//Thread.sleep(2000);
		// Start Date
		driver.findElement(By.xpath("//input[contains(@id,'10C_0')]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//td[text()='13']")).click();

		Thread.sleep(2000);
		// End Date
		driver.findElement(By.xpath("//input[contains(@id,'11C_0')]")).click();

		Thread.sleep(2000);
		driver.findElement(By.xpath("//td[text()='20']")).click();
		
		//customer name
				driver.findElement(By.xpath("//*[contains(@label,'Customer Name')]")).sendKeys("SLK");
				Thread.sleep(1000);
				
				JavascriptExecutor jse1= (JavascriptExecutor) driver;
				jse1.executeScript("scroll(0,750)");
		
		//vertical 
		
		driver.findElement(By.xpath("//input[@label='Vertical']")).sendKeys("vertical");
		
		//current work location of Employee
		
		
		driver.findElement(By.xpath("//input[@label='Current Work Location of Employee']")).sendKeys("Banglore");
		
		
		
		//Immigration status
				driver.findElement(By.xpath("//div[@id='s2id_138C_0']")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//div[@id='select2-drop']/div")).sendKeys("Expired");
				driver.findElement(By.xpath("//div[@id='select2-drop']/div")).sendKeys(Keys.ARROW_DOWN,Keys.ENTER);

		driver.findElement(By.xpath("//button[contains(@class,'save btnf btnf-success text-center pull-right')]"))
				.click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[contains(@id,'btnRuleCheckSubmitCase')]")).click();
		Thread.sleep(20000);
		driver.findElement(By.xpath("//*[@id='modal-alert-ci']/div/div/div[3]/button[1]")).click();
		Thread.sleep(2000);
		
	}
		
	
@Test(priority=3)
	
	public void Screeninglogout()throws InterruptedException  {
		
		Beneficiaryobjects.logouticon.click();
		Thread.sleep(2000);
		Beneficiaryobjects.logoutbutton.click();
		Thread.sleep(2000);
}




		@Test(priority =4)

		public void Beneficiaryaction() throws InterruptedException {
			
			PageFactory.initElements(driver, Lobjects);
			PageFactory.initElements(driver, Advancesearchobj);
			driver.manage().timeouts().implicitlyWait(600, TimeUnit.SECONDS);
			Thread.sleep(3000);
			//driver.get("http://10.10.10.101/enterpriselatest");
			Lobjects.Username.sendKeys("bsix@mailinator.com");
			Lobjects.Password.sendKeys("Password@1");
			Lobjects.signin.click();
			Thread.sleep(5000);
			PageFactory.initElements(driver, Beneficiaryobjects);
			Thread.sleep(2000);
			Beneficiaryobjects.DashboardMytasks.click();
			Thread.sleep(2000);
			Beneficiaryobjects.Mycases.click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//small[text()= '" + x + "' ]")).click();
			Thread.sleep(5000);
			//declaration
			driver.findElement(By.xpath("//button[contains(text(), 'Accept')]")).click();
			Thread.sleep(30000);
			Beneficiaryobjects.Questinnaire.click();
			Beneficiaryobjects.Questinnairecompleted.click();
			Thread.sleep(3000);
			Beneficiaryobjects.Questionnairedialogbox.click();
			Thread.sleep(2000);
			  Beneficiaryobjects.formmodule.click();
			    Thread.sleep(2000);

			Beneficiaryobjects.formcompleted.click();
			Thread.sleep(4000);
			//Beneficiaryobjects.Formsdialogbox.click();
		   Beneficiaryobjects.Questionnairedialogbox.click();
			Thread.sleep(2000);

			Beneficiaryobjects.Documenttab.click();
			Thread.sleep(4000);
			Beneficiaryobjects.DocumentUploaded.click();
			Thread.sleep(2000);
			Beneficiaryobjects.Questionnairedialogbox.click();
			Thread.sleep(3000);
			Beneficiaryobjects.SubmitCase.click();
			Thread.sleep(2000);
		   
		    

		}
		@Test(priority = 5)
		
		public void beneficiarylogout()throws InterruptedException  {
			driver.manage().timeouts().implicitlyWait(600, TimeUnit.SECONDS);
			Beneficiaryobjects.logouticon.click();
			Thread.sleep(2000);
			Beneficiaryobjects.logoutbutton.click();
			Thread.sleep(2000);
		
		}
		
		@Test(priority = 6)
		public void Screeninglogin() throws InterruptedException {
			driver.manage().timeouts().implicitlyWait(600, TimeUnit.SECONDS);
			PageFactory.initElements(driver, Lobjects);
			//driver.get("http://10.10.10.101/enterpriselatest");
			Lobjects.Username.sendKeys("screen@mailinator.com");
			Lobjects.Password.sendKeys("Password@1");
			Lobjects.signin.click();
			Thread.sleep(2000);
		}

		

		@Test(priority=7)
		public void screeningaction() throws InterruptedException {
			PageFactory.initElements(driver, Advancesearchobj);
			PageFactory.initElements(driver, Beneficiaryobjects);
			driver.manage().timeouts().implicitlyWait(600, TimeUnit.SECONDS);
			Advancesearchobj.countrySearchExpand.click();
			Thread.sleep(600);
			Advancesearchobj.SourceCountry.click();
			Advancesearchobj.SourceCountrySearch.sendKeys("India");
			Advancesearchobj.SourceCountrySearch.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
			Advancesearchobj.DestinationCountry.click();
			Advancesearchobj.DestinationCountrySearch.sendKeys("United states");
			Advancesearchobj.DestinationCountrySearch.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
			Thread.sleep(300);
			Advancesearchobj.service.click();
			Advancesearchobj.serviceSearch.sendKeys("Long-Term Transfers");
			Advancesearchobj.serviceSearch.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
			Thread.sleep(700);
			Advancesearchobj.type.click();
			Advancesearchobj.typeSearch.sendKeys("H1B Cap - Non-premium");
			Advancesearchobj.typeSearch.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
			
			
			//to scroll down
			JavascriptExecutor js = (JavascriptExecutor)driver;
			js.executeScript("window.scrollBy(0,450)", "");
			Advancesearchobj.SearchButton.click();
			
			
		    Thread.sleep(3000);
			
			
			
			
			
		//driver.findElement(By.xpath("//*[contains(text(),'Work Allocation')]")).click();
		    //Advancesearchobj.screeningmodule.isDisplayed();
		   // Thread.sleep(2000);
		    //Advancesearchobj.advancesearch.click();
		   // Thread.sleep(2000);
		   // driver.findElement(By.xpath("//a[@caseinitiationid='300063000000001054']")).click();
		    driver.findElement(By.xpath("//a[text()= '" + x + "' ]")).click();
		    Thread.sleep(5000);
			Advancesearchobj.QuestionnaireModule.click();
		   Thread.sleep(4000);
		  Beneficiaryobjects.Questinnairecompleted.click();
		   Thread.sleep(3000);
		    Advancesearchobj.Questdialogbox.click();
		    Thread.sleep(4000);
		    Advancesearchobj.FormModule.click();
		    Thread.sleep(3000);
		    Advancesearchobj.formverified.click();
		    Thread.sleep(5000);
		    Advancesearchobj.Formdialogbox.click();
		    Thread.sleep(2000);
		    Advancesearchobj.DocumentModule.click();
		    Thread.sleep(2000);
		    Advancesearchobj.DocumentVerification.click();
		    Thread.sleep(2000);
		    Advancesearchobj.CaseDecision.click();
		    Thread.sleep(2000);
		    Advancesearchobj.CaseVerfiSelectAction.click();
		    Thread.sleep(3000);
		    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys("screening completed");
		    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
		    Thread.sleep(3000);
		    Advancesearchobj.CaseVerifRemark.sendKeys("Screening completed");
		    Thread.sleep(2000);
		    Advancesearchobj.CaseVerifSave.click();
		    Thread.sleep(4000);
		    
		    
			
			    Advancesearchobj.progressTrackerModule.click();
			    Thread.sleep(4000);
			    Advancesearchobj.progressTrackerModule.click();
			    Thread.sleep(40000);
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("scroll(0,500)");
				Thread.sleep(2000);
				//Advancesearchobj.ProgressTrackexpand.click();
				
				//1 tracker field 
				
				Advancesearchobj.screeningprexpand.click();
				Thread.sleep(4000);
				driver.findElement(By.xpath("//input[@id='date-label-w_1038']")).click();
				driver.findElement(By.xpath("//td[text()='29']")).click();
				Advancesearchobj.savebtn.click();
				Thread.sleep(40000);
				
				
				//2 tracker field
				JavascriptExecutor jse1 = (JavascriptExecutor) driver;
				jse1.executeScript("scroll(0,500)");
				Thread.sleep(2000);
				Advancesearchobj.screeningprexpand.click();
				Thread.sleep(4000);
				driver.findElement(By.xpath("//*[@id='date-label-w_1039']")).click();
				Thread.sleep(2000);
				/*for test 
				
				WebDriverWait wait = new WebDriverWait(driver, 10);
				WebElement Category_Body = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[9]/div[1]/table/tbody/tr[3]/td[text()='15']")));
				 Category_Body.click();
				 driver.switchTo().defaultContent();*/
				 
				 
				driver.findElement(By.xpath("/html/body/div[11]/div[1]/table/tbody/tr[3]/td[4]")).click();
				Advancesearchobj.savebtn.click();
				Thread.sleep(40000);
				
		    //3 tracker field
		    
				JavascriptExecutor jse2 = (JavascriptExecutor) driver;
				jse2.executeScript("scroll(0,550)");
				Thread.sleep(2000);
				Advancesearchobj.screeningprexpand.click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//*[@id='date-label-w_1040']")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("/html/body/div[12]/div[1]/table/tbody/tr[3]/td[2]")).click();
				Advancesearchobj.savebtn.click();
				Thread.sleep(5000);
				
				//screening/processing action
				
				
				 Advancesearchobj.QuestionnaireModule.click();
				    Thread.sleep(4000);
				    Beneficiaryobjects.Questinnairecompleted.click();
				    Thread.sleep(4000);
				    Advancesearchobj.Questdialogbox.click();
				    Thread.sleep(4000);
				    Advancesearchobj.FormModule.click();
				    Thread.sleep(4000);
				    Advancesearchobj.formverified.click();
				    Thread.sleep(4000);
				    Advancesearchobj.Questdialogbox.click();
					Thread.sleep(4000);
					
					 Advancesearchobj.DocumentModule.click();
					 Thread.sleep(4000);
					    Advancesearchobj.DocumentVerification.click();
					   Thread.sleep(4000);
					    Advancesearchobj.CaseDecision.click();
					    Thread.sleep(4000);
					    Advancesearchobj.CaseVerfiSelectAction.click();
					    Thread.sleep(4000);
					    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys("processing Acceptance");
					    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
					    Thread.sleep(4000);
					    Advancesearchobj.CaseVerifRemark.sendKeys("Processing completed");
					    Thread.sleep(2000);
					    Advancesearchobj.CaseVerifSave.click();
					    Thread.sleep(4000);
					    
					    //progress tracker
		    
					    Advancesearchobj.progressTrackerModule.click();
					    Thread.sleep(40000);
						JavascriptExecutor jse3 = (JavascriptExecutor) driver;
						jse3.executeScript("scroll(0,670)");
						Thread.sleep(2000);
				driver.findElement(By.xpath("//a[@id='sd_257']")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//input[@id='date-label-w_1050']")).click();
				Thread.sleep(3000);
				driver.findElement(By.xpath("/html/body/div[13]/div[1]/table/tbody/tr[3]/td[4]")).click();
				Advancesearchobj.savebtn.click();
				Thread.sleep(40000);
		    //2 field
				
				JavascriptExecutor jse4 = (JavascriptExecutor) driver;
				jse4.executeScript("scroll(0,700)");
				Thread.sleep(3000);
		driver.findElement(By.xpath("//a[@id='sd_257']")).click();
		Thread.sleep(2000);
		 driver.findElement(By.xpath("//input[@id='date-label-w_1052']")).click();
		 driver.findElement(By.xpath("/html/body/div[14]/div[1]/table/tbody/tr[3]/td[2]")).click();
			Advancesearchobj.savebtn.click();
			Thread.sleep(40000);
			
			//3 field
			JavascriptExecutor jse5 = (JavascriptExecutor) driver;
			jse5.executeScript("scroll(0,700)");
			Thread.sleep(3000);
	driver.findElement(By.xpath("//a[@id='sd_257']")).click();
	Thread.sleep(2000);
	 driver.findElement(By.xpath("//input[@id='date-label-w_1053']")).click();
	 Thread.sleep(3000);
	 driver.findElement(By.xpath("/html/body/div[15]/div[1]/table/tbody/tr[3]/td[4]")).click();
		Advancesearchobj.savebtn.click();
		Thread.sleep(4000);
		}
		//Lottery selection
		
		@Test(priority=8)
		public void Parrallelaction1() throws InterruptedException {
			PageFactory.initElements(driver, Advancesearchobj);
			PageFactory.initElements(driver, Beneficiaryobjects);
			
		
		Advancesearchobj.CaseDecision.click();
		Thread.sleep(2000);
	    Advancesearchobj.CaseVerfiSelectAction.click();
	    Thread.sleep(2000);
	    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys("Selected in Lottery");
	    Thread.sleep(2000);
	    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys(Keys.ARROW_DOWN,Keys.ARROW_DOWN,Keys.ENTER);
	    Thread.sleep(3000);
	    Advancesearchobj.CaseVerifRemark.sendKeys("Selected in Lottery");
	    Advancesearchobj.CaseVerifSave.click();
	    Thread.sleep(2000);
	    Advancesearchobj.DocumentModule.click();
	    Thread.sleep(4000);
	    Advancesearchobj.DocumentVerification.click();
	    Thread.sleep(2000);
		}
		//Not selected in lottery 
	    
	    @Test(enabled=false)
		public void Parrallelaction1b() throws InterruptedException {
			PageFactory.initElements(driver, Advancesearchobj);
			PageFactory.initElements(driver, Beneficiaryobjects);
			
	    Advancesearchobj.CaseDecision.click();
	    Advancesearchobj.CaseVerfiSelectAction.click();
	    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys("Not-selected in lottery");
	    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
	    Thread.sleep(2000);
	    Advancesearchobj.CaseVerifRemark.sendKeys("Not-selected in lottery");
	    Advancesearchobj.CaseVerifSave.click();
	    Thread.sleep(3000);
	    Advancesearchobj.DocumentModule.click();
	    Advancesearchobj.DocumentVerification.click();
	    Thread.sleep(2000);
	    }
	    
	    //Application approved
	    @Test(priority=10)
		public void Parrallelaction2() throws InterruptedException {
			PageFactory.initElements(driver, Advancesearchobj);
			PageFactory.initElements(driver, Beneficiaryobjects);
	   
	    
	    Advancesearchobj.CaseDecision.click();
	    Thread.sleep(2000);
	    Advancesearchobj.CaseVerfiSelectAction.click();
	    Thread.sleep(2000);
	    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys("Application Approved");
	    Thread.sleep(2000);
	    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
	    Thread.sleep(2000);
	    Advancesearchobj.CaseVerifRemark.sendKeys("Application Approved");
	    Advancesearchobj.CaseVerifSave.click();
	    Thread.sleep(3000);
	    Advancesearchobj.DocumentModule.click();
	    Thread.sleep(4000);
	    Advancesearchobj.DocumentVerification.click();
	    Thread.sleep(2000);
	    }
	  //Application rejected
	    @Test(enabled=false)
		public void Parrallelaction2a() throws InterruptedException {
			
	   
	    
	    PageFactory.initElements(driver, Advancesearchobj);
		PageFactory.initElements(driver, Beneficiaryobjects);
	    Advancesearchobj.CaseDecision.click();
	    Advancesearchobj.CaseVerfiSelectAction.click();
	    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys("Application Rejected");
	    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
	    Thread.sleep(2000);
	    Advancesearchobj.CaseVerifRemark.sendKeys("Application Rejected");
	    Advancesearchobj.CaseVerifSave.click();
	    Thread.sleep(3000);
	    Advancesearchobj.DocumentModule.click();
	    Advancesearchobj.DocumentVerification.click();
	    Thread.sleep(2000);
	    }
	    //Application withdrawn
	    
	    @Test(enabled=false)
		public void Parrallelaction2b() throws InterruptedException {
			PageFactory.initElements(driver, Advancesearchobj);
			PageFactory.initElements(driver, Beneficiaryobjects);
	    
	    Advancesearchobj.CaseDecision.click();
	    Advancesearchobj.CaseVerfiSelectAction.click();
	    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys("Application withdrawn");
	    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
	    Thread.sleep(2000);
	    Advancesearchobj.CaseVerifRemark.sendKeys("Application withdrawn");
	    Advancesearchobj.CaseVerifSave.click();
	    Thread.sleep(3000);
	    Advancesearchobj.DocumentModule.click();
	    Advancesearchobj.DocumentVerification.click();
	    Thread.sleep(2000);
	    }
	    //RFE Received
	    @Test(enabled=false)
		public void Parrallelaction3() throws InterruptedException {
			PageFactory.initElements(driver, Advancesearchobj);
			PageFactory.initElements(driver, Beneficiaryobjects);
	   
	    Advancesearchobj.CaseDecision.click();
	    Advancesearchobj.CaseVerfiSelectAction.click();
	    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys("RFE Received");
	    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
	    Thread.sleep(2000);
	    Advancesearchobj.CaseVerifRemark.sendKeys("RFE Received");
	    Advancesearchobj.CaseVerifSave.click();
	    Thread.sleep(3000);
	    Advancesearchobj.QuestionnaireModule.click();
	    Thread.sleep(600);
	    Beneficiaryobjects.Questinnairecompleted.click();
	    Thread.sleep(2000);
	    Advancesearchobj.Questdialogbox.click();
	    Thread.sleep(3000);
	    
	    
	}
		

		
}
		
		
		
		
		
		
		
		
		
	
	


