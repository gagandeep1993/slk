package CaseInitiation;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.sikuli.script.SikuliException;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import BeneficiaryPage.ApplyforVisaobjects;
import ManagerPage.Approvalobjects;
import ScreeningPage.AdvanceSearchPage;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import login.Loginobjects;

public class US_ST_BV {
	public WebDriver driver;
	String x;
	String a ;

		
	Loginobjects Lobjects = new Loginobjects();
	ApplyforVisaobjects Beneficiaryobjects = new ApplyforVisaobjects();
	Approvalobjects Approvalobj=new Approvalobjects();
    AdvanceSearchPage Advancesearchobj=new AdvanceSearchPage();
    
    
	@BeforeTest
	public void InitiateBrowser() {
		
		System.setProperty("webdriver.gecko.driver", "C:\\Enterprisefile\\geckodriver-v0.19.0-win64.exe");
		driver = new FirefoxDriver();
		
		//Chrome browser
		
		/*System.setProperty("webdriver.chrome.driver", "C:\\Users\\gagandeep.k\\Downloads\\chromedriver64.exe");
		driver = new ChromeDriver();*/
	
	}

	@Test(priority = 1)
	public void login() throws InterruptedException {
		
		
		
		//SoftAssert softasserts = new SoftAssert();
		PageFactory.initElements(driver, Lobjects);
		driver.get("http://10.10.10.101/enterpriseqa/");
	
		Lobjects.Username.sendKeys("bsix@mailinator.com");
		Lobjects.Password.sendKeys("Password@1");
		
		String loginpassword = Lobjects.Password.getAttribute("value");
		Assert.assertEquals(loginpassword,"Password@1" );
		//Assert.assertEquals(actual, Expected);
		Lobjects.Password.sendKeys(Keys.ENTER);
		
		
		Lobjects.signin.click();
		
		
		
		Thread.sleep(50000);
		
	}
	
	//CASE INITIATION
	@Test(priority = 2)

	public void Initiatecase() throws InterruptedException, SikuliException, BiffException, IOException {
		//driver.manage().timeouts().implicitlyWait(600,TimeUnit.SECONDS );
	    
		
	
		PageFactory.initElements(driver, Beneficiaryobjects);
		Beneficiaryobjects.DashboardApplyforvisa.click();
		
		Thread.sleep(2000);
		Beneficiaryobjects.residingCountry.click();
		Thread.sleep(2000);
		Beneficiaryobjects.residingCountrysearch.sendKeys("India");
		Thread.sleep(2000);
		
		Beneficiaryobjects.residingCountrysearch.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
		Thread.sleep(1000);
		Beneficiaryobjects.DestinationCountry.click();
		Thread.sleep(1000);
		Beneficiaryobjects.DestinationCountry.sendKeys("united states");
		
		//Sikuli 
		
		Screen screen =new Screen();
	Pattern DestinationImage = new Pattern("C:\\Users\\gagandeep.k\\Documents\\Gagan\\Selenium\\Images\\Destination country.png");
		
	screen.wait(DestinationImage, 20);
	screen.click(DestinationImage);
	
				
			
		
		Thread.sleep(2000);
		//Beneficiaryobjects.DestinationCountry.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
		//Thread.sleep(2000);
		Beneficiaryobjects.TravelPurpose.click();
		Beneficiaryobjects.TravelPurposeSearch.sendKeys("classroom Training");
		Beneficiaryobjects.TravelPurposeSearch.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
		Beneficiaryobjects.Description.sendKeys("Classroom Training");
		Thread.sleep(2000);
		Beneficiaryobjects.passport.click();
		Thread.sleep(2000);
		Beneficiaryobjects.passportsearch.sendKeys("IPF2343242");
		Thread.sleep(2000);
		Beneficiaryobjects.passportsearch.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
		Thread.sleep(2000);
		Beneficiaryobjects.Initiate.click();
		Thread.sleep(6000);
		x = driver.findElement(By.xpath("//p[@id='pCIID']")).getText();
		System.out.println("The case no is" + x);
		Thread.sleep(15000);
		

		// scroll page down till Address1 button for firefox.
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("scroll(0,650)");
		Thread.sleep(4000);
		
		//case initiation fields
		
		//billability status
		
	    driver.findElement(By.xpath("//div[@id='s2id_421C_0']")).click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//div[@id='select2-drop']/div/input")).sendKeys("non-billable");
		driver.findElement(By.xpath("//div[@id='select2-drop']/div/input")).sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
		Thread.sleep(1000);
		
		//priority
		
		driver.findElement(By.xpath("//div[@id='s2id_3809C_0']")).click();
		driver.findElement(By.xpath("//div[@id='select2-drop']/div/input")).sendKeys("High");
		driver.findElement(By.xpath("//div[@id='select2-drop']/div/input")).sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
		Thread.sleep(1000);
		
		// Start Date
		driver.findElement(By.xpath("//input[contains(@id,'10C_0')]")).click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//td[text()='20']")).click();
		/*Workbook wb = Workbook.getWorkbook(new File("C:\\Users\\gagandeep.k\\Documents\\Gagan\\Selenium\\Testdata\\Dates.xls"));
		Sheet s1 = wb.getSheet(0);
				
		String DateOfBirth = s1.getCell(0,0).getContents();
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("document.getElementById('10C_0').value='"+DateOfBirth+"'");*/
Thread.sleep(2000);
		
        // End Date


		
/*String DateOfBirth2 = s1.getCell(1,0).getContents();
JavascriptExecutor jsDOB = (JavascriptExecutor)driver;
jsDOB.executeScript("document.getElementById('11C_0').value='"+DateOfBirth2+"'");*/
Thread.sleep(1000);

   driver.findElement(By.xpath("//input[contains(@id,'11C_0')]")).click();
    driver.findElement(By.xpath("//td[text()='23']")).click();
     Thread.sleep(1000);
     
     /*state
     
     driver.findElement(By.xpath("//div[@id='s2id_22C_0_1']")).click();
     driver.findElement(By.xpath("//div[@id='select2-drop']/div/input")).sendKeys("bavaria");
     driver.findElement(By.xpath("//div[@id='select2-drop']/div/input")).sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
     Thread.sleep(1000);
     //city
     
     driver.findElement(By.xpath("//div[@id='s2id_22C_0_2']")).click();
     driver.findElement(By.xpath("//div[@id='select2-drop']/div/input")).sendKeys("Abenberg");
     driver.findElement(By.xpath("//div[@id='select2-drop']/div/input")).sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
     Thread.sleep(1000);
     
     //Address1
     
   driver.findElement(By.xpath("//input[contains(@label,'Address 1')]")).sendKeys("Banglore");
   		Thread.sleep(1000);
   		
   		//Address 2
   		driver.findElement(By.xpath("//input[contains(@label,'Address 2')]")).sendKeys("Jammu");
   		Thread.sleep(1000);
   		
        //Project name
	driver.findElement(By.xpath("//*[contains(@label,'Project Name')]")).sendKeys("Enterprise");
		Thread.sleep(1000);
     
   		
   	//customer name
   	driver.findElement(By.xpath("//*[contains(@label,'Customer Name')]")).sendKeys("SLK");
   			Thread.sleep(500);*/
		
   //current work location of Employee
   		driver.findElement(By.xpath("//input[@label='Current Work Location of Employee']")).sendKeys("Banglore");
   		
   		//salary per month
   		driver.findElement(By.xpath("//input[@id='543C_0']")).sendKeys("100000");
		
		
		
		
				Thread.sleep(500);
				//yearly income
				
				driver.findElement(By.xpath("//input[@label='Yearly Income in India / Onsite']")).sendKeys("23890");
				
				Thread.sleep(1000);
				
		
		
		
		
		JavascriptExecutor jse10 = (JavascriptExecutor) driver;
		jse10.executeScript("scroll(0,750)");
		

		Thread.sleep(900);
		
		//previous travel status
		
		
		
		//Immigration status
		driver.findElement(By.xpath("//div[@id='s2id_138C_0']")).click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//div[@id='select2-drop']/div")).sendKeys("Expired");
		driver.findElement(By.xpath("//div[@id='select2-drop']/div")).sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
		
		Thread.sleep(1000);
		JavascriptExecutor jse12 = (JavascriptExecutor) driver;
		jse12.executeScript("scroll(0,900)");
		
		//Project code
		driver.findElement(By.xpath("//div[@class='select2-container questionnaire-drpDwn dropdown js-example-responsive validation CyientProjectCode drpWidth']")).click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//div[@id='select2-drop']/div/input")).sendKeys("santoshcbu@immidartgroup.com");
		driver.findElement(By.xpath("//div[@id='select2-drop']/div/input")).sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
		Thread.sleep(500);

		
		
		driver.findElement(By.xpath("//button[contains(@class,'save btnf btnf-success text-center pull-right')]"))
				.click();
		
Thread.sleep(2000);
		//vertical 
		
		/*driver.findElement(By.xpath("//input[@label='Vertical']")).sendKeys("vertical");
		Thread.sleep(500);*/
		
			//check boxes
		
		JavascriptExecutor jse11 = (JavascriptExecutor) driver;
		jse11.executeScript("scroll(0,2000)");
		
		
		driver.findElement(By.xpath("//input[@id='1']")).click();
		driver.findElement(By.xpath("//input[@id='2']")).click();
		driver.findElement(By.xpath("//input[@id='3']")).click();
		driver.findElement(By.xpath("//input[@id='4']")).click();
		driver.findElement(By.xpath("//input[@id='5']")).click();
		driver.findElement(By.xpath("//input[@id='6']")).click();
		Thread.sleep(1000);
		

		
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[contains(@id,'btnRuleCheckSubmitCase')]")).click();
		Thread.sleep(10000);
		driver.findElement(By.xpath("//*[@id='modal-alert-ci']/div/div/div[3]/button[1]")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//small[text()= '" + x + "' ]")).click();
		Thread.sleep(5000);
		//declaration
		driver.findElement(By.xpath("//button[contains(text(), 'Accept')]")).click();
		Thread.sleep(50000);

		// Summary Page.
		Beneficiaryobjects.Questinnaire.click();
		Thread.sleep(1000);
		Beneficiaryobjects.Questinnairecompleted.click();
		Thread.sleep(2000);
		Beneficiaryobjects.Questionnairedialogbox.click();
		Thread.sleep(2000);

		Beneficiaryobjects.AppoinmentQuestinnaire.click();
		Thread.sleep(3000);
		Beneficiaryobjects.AppoinmentQuestionnairecompleted.click();
		Thread.sleep(2000);
		Beneficiaryobjects.Questionnairedialogbox.click();
		Thread.sleep(2000);

		Beneficiaryobjects.Documenttab.click();
		Thread.sleep(500);
		Beneficiaryobjects.DocumentUploaded.click();
		Thread.sleep(2000);
		Beneficiaryobjects.Questionnairedialogbox.click();
		Thread.sleep(3000);
		Beneficiaryobjects.SubmitCase.click();
		Thread.sleep(1000);
		
		//Capturing ToasterMSG
	    /*WebDriverWait wait = new WebDriverWait(driver, 500);
		WebElement ele = driver.findElement(By.id("toast-container"));
		wait.until(ExpectedConditions.visibilityOf(ele));
		String text = driver.findElement(By.xpath("//div[@class='toast-message']")).getText();
		System.out.println("-----------------------------------------------------------------");
		System.out.println(text);
		System.out.println("-----------------------------------------------------------------");
		Thread.sleep(3000);
		Assert.assertEquals(text, "case is submitted successfully");
		Thread.sleep(1000);*/
		
		
	}
	@Test(priority=3)
	
	public void Beneficiarylogout() {
		
		Beneficiaryobjects.logouticon.click();
		Beneficiaryobjects.logoutbutton.click();
		
	}

	@Test(priority=4)
	public void managerlogin() throws InterruptedException {
		Lobjects.Username.sendKeys("santoshcbu@immidartgroup.com");
		Lobjects.Password.sendKeys("password");
		Lobjects.signin.click();
		Thread.sleep(40000);
	}
	
		@Test(priority=5)
		public void manageraction() throws InterruptedException {
			PageFactory.initElements(driver, Approvalobj);
			Thread.sleep(3000);
		//driver.findElement(By.xpath("//div[text()='Approval']")).click();
		Approvalobj.ApprovalDashboard.click();
		Thread.sleep(600);
		Approvalobj.AwaitingforApproval.click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//a[text()= '" + x + "' ]")).click();
		Thread.sleep(4000);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("scroll(0,550)");
		Thread.sleep(3000);
		
		//check boxes
		driver.findElement(By.xpath("//input[@id='1']")).click();
		driver.findElement(By.xpath("//input[@id='2']")).click();
		driver.findElement(By.xpath("//input[@id='3']")).click();
		driver.findElement(By.xpath("//input[@id='4']")).click();
		driver.findElement(By.xpath("//input[@id='5']")).click();
		
		Approvalobj.Approvestatus.click();
		Approvalobj.statusdropdown.sendKeys("Approve");
		Approvalobj.statusdropdown.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
		Thread.sleep(500);
		Approvalobj.Remarks.sendKeys("CaseApproved");
		Thread.sleep(500);
		Approvalobj.submit.click();
		Thread.sleep(15000);
		Approvalobj.caseapproved.click();
		}
		//manager logout
		@Test(priority=6)
		public void managerlogout() {
			
			Beneficiaryobjects.logouticon.click();
			Beneficiaryobjects.logoutbutton.click();
			
		}
		
		//screening
		
		@Test(priority=7)
		
		public void screeeninglogin() throws InterruptedException {
			
			Lobjects.Username.sendKeys("screen@mailinator.com");
			Lobjects.Password.sendKeys("Password@1");
			Lobjects.signin.click();
			Thread.sleep(600);
			}
		
		
		@Test(priority=8)
		public void screeningaction() throws InterruptedException {
			
		//	driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			PageFactory.initElements(driver, Advancesearchobj);
			PageFactory.initElements(driver, Beneficiaryobjects);
			Advancesearchobj.countrySearchExpand.click();
			Thread.sleep(600);
			Advancesearchobj.SourceCountry.click();
			Advancesearchobj.SourceCountrySearch.sendKeys("India");
			Advancesearchobj.SourceCountrySearch.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
			Advancesearchobj.DestinationCountry.click();
			Advancesearchobj.DestinationCountrySearch.sendKeys("United states");
			Advancesearchobj.DestinationCountrySearch.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
			Thread.sleep(300);
			Advancesearchobj.service.click();
			Advancesearchobj.serviceSearch.sendKeys("short-Term Transfers");
			Advancesearchobj.serviceSearch.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
			Advancesearchobj.type.click();
			Advancesearchobj.typeSearch.sendKeys("business Visa");
			Advancesearchobj.typeSearch.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
			
			//to scroll down
			JavascriptExecutor js = (JavascriptExecutor)driver;
			js.executeScript("window.scrollBy(0,450)", "");
			Advancesearchobj.SearchButton.click();
			
			
		    Thread.sleep(2000);
		   // JavascriptExecutor js1 = (JavascriptExecutor)driver;
			//js1.executeScript("window.scrollBy(0,650)", "");
		    //driver.findElement(By.xpath("//a[@caseinitiationid='200062000000001025']")).click();
		   driver.findElement(By.xpath("//a[text()= '" + x + "' ]")).click();
		    Thread.sleep(5000);
		    //update immigration status
		    
		   /*driver.findElement(By.xpath("//div[@id='s2id_138C_0']")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//div[@id='select2-drop']/div")).sendKeys("Expired");
			Thread.sleep(2000);
			driver.findElement(By.xpath("//div[@id='select2-drop']/div")).sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
		   Thread.sleep(3000);*/
			Advancesearchobj.QuestionnaireModule.click();
		   Thread.sleep(4000);
		  Beneficiaryobjects.Questinnairecompleted.click();
		   Thread.sleep(2000);
		    Advancesearchobj.Questdialogbox.click();
		    Thread.sleep(2000);
		    Advancesearchobj.DocumentModule.click();
		    Thread.sleep(3000);
		    Advancesearchobj.DocumentVerification.click();
		    Thread.sleep(4000);
		    Advancesearchobj.CaseDecision.click();
		    Thread.sleep(2000);
		    Advancesearchobj.CaseVerfiSelectAction.click();
		    Thread.sleep(2000);
		    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys("screening completed");
		    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
		    Thread.sleep(2000);
		    Advancesearchobj.CaseVerifRemark.sendKeys("Screening completed");
		    Thread.sleep(2000);
		    Advancesearchobj.CaseVerifSave.click();
		    Thread.sleep(2000);
		    driver.navigate().refresh();
		    Thread.sleep(3000);
		    Advancesearchobj.CaseDecision.click();
		    Thread.sleep(2000);
		    //----Personal interview----//
		     Advancesearchobj.CaseVerfiSelectAction.click();
		    Thread.sleep(2000);
		    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys("personal interview");
		    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);

		    Advancesearchobj.CaseVerifRemark.sendKeys("personal interview");
		    Thread.sleep(2000);
		    Advancesearchobj.CaseVerifSave.click();
		    Thread.sleep(4000);
		    Advancesearchobj.Appoinmentmodule.click();
		    Thread.sleep(4000);
		    Advancesearchobj.ApponmentQuestionnaireverified.click();
		    Thread.sleep(4000);
		    Advancesearchobj.Questdialogbox.click();
		    Thread.sleep(4000);
		    Advancesearchobj.DocumentModule.click();
		    Thread.sleep(4000);
		    Advancesearchobj.DocumentVerification.click();
		    
		}
		  //-----Drop box----//
		    
		    @Test(enabled=false)
		    
			public void Parallelaction() throws InterruptedException {
		    	
		    
		    PageFactory.initElements(driver, Advancesearchobj);
			PageFactory.initElements(driver, Beneficiaryobjects);
		    Advancesearchobj.CaseVerfiSelectAction.click();
		    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys("Drop box");
		    Advancesearchobj.CaseVerifRemark.sendKeys("Drop box");
		    Advancesearchobj.CaseVerifSave.click();
		    Thread.sleep(500);
		    Advancesearchobj.Appoinmentmodule.click();
		    Advancesearchobj.ApponmentQuestionnaireverified.click();
		    Thread.sleep(500);
		    Advancesearchobj.Questdialogbox.click();
		    Advancesearchobj.DocumentModule.click();
		    Advancesearchobj.DocumentVerification.click();
		    Thread.sleep(2000);
		    
		    }
		    
		    
		    //----Appoinment confirm----//
		    
		    @Test(priority=9)
			public void Parallelaction2() throws InterruptedException {
		    	
		    
		    PageFactory.initElements(driver, Advancesearchobj);
			PageFactory.initElements(driver, Beneficiaryobjects);
		    Thread.sleep(2000);
		Advancesearchobj.CaseDecision.click();
		    Thread.sleep(2000);
		    Advancesearchobj.CaseVerfiSelectAction.click();
		    Thread.sleep(2000);
		    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys("Appointment confirm");
		    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
		    Thread.sleep(2000);
		    Advancesearchobj.CaseVerifRemark.sendKeys("Appointment confirm");
		    Thread.sleep(2000);
		    Advancesearchobj.CaseVerifSave.click();
		    Thread.sleep(4000);
		    Advancesearchobj.progressTrackerModule.click();
		    //Thread.sleep(4000);
		   // Advancesearchobj.progressTrackerModule.click();
		   Thread.sleep(50000);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("scroll(0,650)");
			Thread.sleep(5000);
			Advancesearchobj.ProgressTrackexpand.click();
			Thread.sleep(2000);
			Advancesearchobj.Trackerfield.click();
			driver.findElement(By.xpath("//td[text()='21']")).click();
			Advancesearchobj.savebtn.click();
			Thread.sleep(7000);
			// Advancesearchobj.progressTrackerModule.click();
			   //Thread.sleep(4000);
			JavascriptExecutor jse1 = (JavascriptExecutor) driver;
			jse1.executeScript("scroll(0,990)");
			Thread.sleep(4000);
			Advancesearchobj.ProgressTrackexpand.click();
			Thread.sleep(5000);
			//WebElement element= driver.findElement(By.xpath("//input[@id='date-label-w_1017']"));
			//((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
			Advancesearchobj.Trackerfield2.click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("/html/body/div[11]/div[1]/table/tbody/tr[3]/td[4]")).click();
			//Thread.sleep(2000);
			Advancesearchobj.savebtn.click();
			Thread.sleep(5000);
		    }
			//----Application approved-----//
			
			 @Test(priority=10)
		     public void Parallelaction3() throws InterruptedException {
			    	
			    
			    PageFactory.initElements(driver, Advancesearchobj);
				PageFactory.initElements(driver, Beneficiaryobjects);
			    
			Advancesearchobj.CaseDecision.click();
			 Thread.sleep(2000);
		    Advancesearchobj.CaseVerfiSelectAction.click();
		    Thread.sleep(2000);
		    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys("Application Approved");
		    Thread.sleep(2000);
		    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
		    Thread.sleep(500);
		    Advancesearchobj.CaseVerifRemark.sendKeys("Application Approved");
		    Thread.sleep(2000);
		    Advancesearchobj.CaseVerifSave.click();
		    Thread.sleep(2000);
		    Advancesearchobj.DocumentModule.click();
		    Thread.sleep(2000);
		    Advancesearchobj.DocumentVerification.click();
		    Thread.sleep(2000);
		    
		    Advancesearchobj.progressTrackerModule.click();
			 Thread.sleep(5000);
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("scroll(0,2000)");
				Thread.sleep(4000);
				Advancesearchobj.Approvalcopyuploadexpand.click();
				Thread.sleep(2000);
				Advancesearchobj.Trackerfield3.click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("/html/body/div[12]/div[1]/table/tbody/tr[3]/td[2]")).click();
				Advancesearchobj.savebtn.click();
			 }
			//---Admin Process------
			
		    @Test(enabled =false)
			public void Parallelaction4() throws InterruptedException {
		    	
		    
		    PageFactory.initElements(driver, Advancesearchobj);
			PageFactory.initElements(driver, Beneficiaryobjects);
		    Advancesearchobj.CaseDecision.click();
		    Advancesearchobj.CaseVerfiSelectAction.click();
		    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys("Admin Process");
		    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
		    Thread.sleep(500);
		    Advancesearchobj.CaseVerifRemark.sendKeys("Admin Process");
		    Advancesearchobj.CaseVerifSave.click();
		    Thread.sleep(2000);
		    Advancesearchobj.DocumentModule.click();
		    Advancesearchobj.DocumentVerification.click();
		    Thread.sleep(500);
		    }
           //---Application Rejected------
		    @Test(enabled =false)
			public void Parallelaction5() throws InterruptedException {
		    	
		    
		    PageFactory.initElements(driver, Advancesearchobj);
			PageFactory.initElements(driver, Beneficiaryobjects);
		    Advancesearchobj.CaseDecision.click();
		    Advancesearchobj.CaseVerfiSelectAction.click();
		    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys("Application Rejected");
		    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
		    Thread.sleep(500);
		    Advancesearchobj.CaseVerifRemark.sendKeys("Application Rejected");
		    Advancesearchobj.CaseVerifSave.click();
		    Thread.sleep(2000);
		    Advancesearchobj.DocumentModule.click();
		    Advancesearchobj.DocumentVerification.click();
		    Thread.sleep(500);
		    
		    }
 //---Application withdrawn------
		    @Test(enabled =false)
			public void Parallelaction6() throws InterruptedException {
		    	
		    
		    PageFactory.initElements(driver, Advancesearchobj);
			PageFactory.initElements(driver, Beneficiaryobjects);
		    Advancesearchobj.CaseDecision.click();
		    Advancesearchobj.CaseVerfiSelectAction.click();
		    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys("Application withdrawn");
		    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
		    Thread.sleep(500);
		    Advancesearchobj.CaseVerifRemark.sendKeys("Application withdrawn");
		    Advancesearchobj.CaseVerifSave.click();
		    Thread.sleep(2000);
		    Advancesearchobj.DocumentModule.click();
		    Advancesearchobj.DocumentVerification.click();
		    Thread.sleep(500);
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		    
		}
		
		
		
		
		
	}
	
	
	
	
	
	

