package CaseInitiation;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import BeneficiaryPage.ApplyforVisaobjects;
import ManagerPage.Approvalobjects;
import ScreeningPage.AdvanceSearchPage;
import ScreeningPage.OrganizationPage;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import login.Loginobjects;

public class GM_LT_WorkPermitD {
	
	public WebDriver driver;
	String x;
	Loginobjects Lobjects = new Loginobjects();
	ApplyforVisaobjects Beneficiaryobjects = new ApplyforVisaobjects();
	OrganizationPage Orgobjects= new OrganizationPage();
	 AdvanceSearchPage Advancesearchobj=new AdvanceSearchPage();
	 Approvalobjects Approvalobj=new Approvalobjects();
	 
	 
	@BeforeTest
	public void InitiateBrowser() {
		System.setProperty("webdriver.gecko.driver", "C:\\Enterprisefile\\geckodriver-v0.19.0-win64.exe");
		driver = new FirefoxDriver();
	}

	@Test(priority = 1)
	public void login() throws InterruptedException {

		PageFactory.initElements(driver, Lobjects);
		driver.get("http://10.10.10.101/enterpriseqa");
		Lobjects.Username.sendKeys("tl1@mailinator.com");
		Lobjects.Password.sendKeys("Password@1");
		Lobjects.signin.click();
		//Thread.sleep(40000);
	}
	@Test(priority=2)
	public void Initiatecase() throws InterruptedException, FindFailed, BiffException, IOException  {
		//driver.manage().timeouts().implicitlyWait(600, TimeUnit.SECONDS);
		PageFactory.initElements(driver, Beneficiaryobjects);
		PageFactory.initElements(driver, Orgobjects);
		
		/*Orgobjects.OrganizationModule.click();
		Thread.sleep(4000);
		//Orgobjects.clientsubmodule.click();
		//Thread.sleep(3000);
		Orgobjects.Addicon.click();
		Thread.sleep(3000);
		Orgobjects.Plus.click();
		Thread.sleep(3000);
		Orgobjects.Searchcriteria.click();
		Thread.sleep(3000);
		Orgobjects.Searchdropdown.sendKeys("beneficiary Email");
		Thread.sleep(3000);
		Orgobjects.Searchdropdown.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
		Thread.sleep(3000);
		Orgobjects.Selectcondition.click();
		Orgobjects.Selectdropdown.sendKeys("Equals");
		Thread.sleep(3000);
		Orgobjects.Selectdropdown.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
		Thread.sleep(3000);
		Orgobjects.Textbox.sendKeys("bone@mailinator.com");
		Thread.sleep(3000);
		Orgobjects.searchbutton.click();
		Thread.sleep(700);
		Orgobjects.BeneficiaryEmailid.click();
		Thread.sleep(3000);
		Orgobjects.Addnewcase.click();*/
		Thread.sleep(10000);
		//driver.findElement(By.xpath("//div[text()='Apply For Visa']")).click();
		Beneficiaryobjects.DashboardApplyforvisa.click();
		
		Thread.sleep(2000);
		Beneficiaryobjects.residingCountry.click();
		Thread.sleep(3000);
		Beneficiaryobjects.residingCountrysearch.sendKeys("India");
		Thread.sleep(2000);
		Beneficiaryobjects.residingCountrysearch.sendKeys(Keys.ARROW_DOWN, Keys.ENTER,Keys.ARROW_DOWN, Keys.ENTER);
		Beneficiaryobjects.DestinationCountry.click();
		Thread.sleep(3000);
		Beneficiaryobjects.DestinationCountry.sendKeys("Germany");
		Thread.sleep(2000);
		
		//Sikuli 
		
				Screen screen =new Screen();
			Pattern DestinationImage = new Pattern("C:\\Users\\gagandeep.k\\Documents\\Gagan\\Selenium\\Images\\Germany.png");
				
			screen.wait(DestinationImage, 20);
			screen.click(DestinationImage);
			
		//Beneficiaryobjects.DestinationCountry.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
		Thread.sleep(3000);
		//Beneficiaryobjects.TravelPurpose.click();
		//Beneficiaryobjects.TravelPurposeSearch.sendKeys("classroom Training");
		//Beneficiaryobjects.TravelPurposeSearch.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
		//Beneficiaryobjects.Description.sendKeys("Classroom Training");
		Beneficiaryobjects.passport.click();
		Beneficiaryobjects.passportsearch.sendKeys("we345sdf");
		Beneficiaryobjects.passportsearch.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
		Thread.sleep(4000);
		Beneficiaryobjects.Initiate.click();
		Thread.sleep(10000);
		
		x = driver.findElement(By.xpath("//p[@id='pCIID']")).getText();
		System.out.println("The case no is" + x);
		Thread.sleep(20000);

		// scroll page down till Address1 button for firefox.
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("scroll(0,450)");
		Thread.sleep(4000);
		
		//case initiation fields
			
				//priority
				
				driver.findElement(By.xpath("//div[@id='s2id_3809C_0']")).click();
				driver.findElement(By.xpath("//div[@id='select2-drop']/div/input")).sendKeys("High");
				driver.findElement(By.xpath("//div[@id='select2-drop']/div/input")).sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
				Thread.sleep(1000);
				
				// Start Date
				driver.findElement(By.xpath("//input[contains(@id,'10C_0')]")).click();
				Thread.sleep(500);
				driver.findElement(By.xpath("//td[text()='20']")).click();
				/*Workbook wb = Workbook.getWorkbook(new File("C:\\Users\\gagandeep.k\\Documents\\Gagan\\Selenium\\Testdata\\Dates.xls"));
				Sheet s1 = wb.getSheet(0);
						
				String DateOfBirth = s1.getCell(0,0).getContents();
				JavascriptExecutor js = (JavascriptExecutor)driver;
				js.executeScript("document.getElementById('10C_0').value='"+DateOfBirth+"'");*/
		Thread.sleep(2000);
				
		        // End Date


				
		/*String DateOfBirth2 = s1.getCell(1,0).getContents();
		JavascriptExecutor jsDOB = (JavascriptExecutor)driver;
		jsDOB.executeScript("document.getElementById('11C_0').value='"+DateOfBirth2+"'");*/
		Thread.sleep(2000);

		   driver.findElement(By.xpath("//input[contains(@id,'11C_0')]")).click();
		    driver.findElement(By.xpath("//td[text()='23']")).click();
		     Thread.sleep(2000);
		     
		     //state
		     
		     driver.findElement(By.xpath("//div[@id='s2id_22C_0_1']")).click();
		     driver.findElement(By.xpath("//div[@id='select2-drop']/div/input")).sendKeys("bavaria");
		     driver.findElement(By.xpath("//div[@id='select2-drop']/div/input")).sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
		     Thread.sleep(1000);
		     //city
		     
		     driver.findElement(By.xpath("//div[@id='s2id_22C_0_2']")).click();
		     driver.findElement(By.xpath("//div[@id='select2-drop']/div/input")).sendKeys("Abenberg");
		     driver.findElement(By.xpath("//div[@id='select2-drop']/div/input")).sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
		     Thread.sleep(1000);
		     
		     //Address1
		     
		   driver.findElement(By.xpath("//input[contains(@label,'Address 1')]")).sendKeys("Banglore");
		   		Thread.sleep(1000);
		   		
		   		//Address 2
		   		driver.findElement(By.xpath("//input[contains(@label,'Address 2')]")).sendKeys("Jammu");
		   		Thread.sleep(1000);
		   		
		   	// scroll page down till Address1 button for firefox.
				JavascriptExecutor jse1 = (JavascriptExecutor) driver;
				jse1.executeScript("scroll(0,650)");
				Thread.sleep(4000);
		   		
		   		
		   	//customer name
			   	driver.findElement(By.xpath("//*[contains(@label,'Customer Name')]")).sendKeys("SLK");
			   			Thread.sleep(500);

		   		
		        //Project name
			driver.findElement(By.xpath("//*[contains(@label,'Project Name')]")).sendKeys("Enterprise");
				Thread.sleep(1000);
				
				
				//Project code
				driver.findElement(By.xpath("//div[@class='select2-container questionnaire-drpDwn dropdown js-example-responsive validation CyientProjectCode drpWidth']")).click();
				Thread.sleep(500);
				driver.findElement(By.xpath("//div[@id='select2-drop']/div/input")).sendKeys("santoshproject@immidartgroup.com");
				driver.findElement(By.xpath("//div[@id='select2-drop']/div/input")).sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
				Thread.sleep(500);
		     
//vertical 
				
				driver.findElement(By.xpath("//input[@label='Vertical']")).sendKeys("vertical");
				Thread.sleep(500);	
				
		   		
		   
				
		   //current work location of Employee
		   		driver.findElement(By.xpath("//input[@label='Current Work Location of Employee']")).sendKeys("Banglore");
		  
		   		//Immigration status
				driver.findElement(By.xpath("//div[@id='s2id_138C_0']")).click();
				Thread.sleep(500);
				driver.findElement(By.xpath("//div[@id='select2-drop']/div")).sendKeys("Expired");
				driver.findElement(By.xpath("//div[@id='select2-drop']/div")).sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
				
				Thread.sleep(3000);
				
				
				driver.findElement(By.xpath("//button[contains(@class,'save btnf btnf-success text-center pull-right')]"))
				.click();

				

		   Thread.sleep(3000);
		
		
		
		driver.findElement(By.xpath("//button[contains(@id,'btnRuleCheckSubmitCase')]")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='modal-alert-ci']/div/div/div[3]/button[1]")).click();
		Thread.sleep(2000);
		

	}
	
	@Test(priority=3)
	public void teamleadlogout() {
		
		Beneficiaryobjects.logouticon.click();
		Beneficiaryobjects.logoutbutton.click();
		
	}
	
	//first level of Approval - project manager
	
	@Test(priority=4)
	public void managerlogin() throws InterruptedException {
		Lobjects.Username.sendKeys("santoshproject@immidartgroup.com");
		Lobjects.Password.sendKeys("password");
		Lobjects.signin.click();
		Thread.sleep(3000);
	}
	
		@Test(priority=5)
		public void manageraction() throws InterruptedException {
			PageFactory.initElements(driver, Approvalobj);
			Thread.sleep(10000);
			//driver.findElement(By.xpath("//div[text()='Approval']")).click();
		Approvalobj.ApprovalDashboard.click();
		Thread.sleep(3000);
		Approvalobj.AwaitingforApproval.click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//a[text()= '" + x + "' ]")).click();
		Thread.sleep(5000);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("scroll(0,650)");
		Thread.sleep(3000);
		
		
		
		
		
		Approvalobj.Approvestatus.click();
		Approvalobj.statusdropdown.sendKeys("Approve");
		Approvalobj.statusdropdown.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
		Thread.sleep(3000);
		Approvalobj.Remarks.sendKeys("CaseApproved");
		Thread.sleep(3000);
		Approvalobj.submit.click();
		Thread.sleep(6000);
		Approvalobj.caseapproved.click();
		}
		//manager logout
		@Test(priority=6)
		public void managerlogout() {
			
			Beneficiaryobjects.logouticon.click();
			Beneficiaryobjects.logoutbutton.click();
			
		}
		//2nd approval - finance manager login
		
		@Test(priority=7)
		public void managerlogin2() throws InterruptedException {
			Lobjects.Username.sendKeys("santoshfinance@immidartgroup.com");
			Lobjects.Password.sendKeys("password");
			Lobjects.signin.click();
			Thread.sleep(3000);
		}	
			@Test(priority=8)
			public void manageraction2() throws InterruptedException {
				
				PageFactory.initElements(driver, Approvalobj);
				Thread.sleep(10000);
				//driver.findElement(By.xpath("//div[text()='Approval']")).click();
			Approvalobj.ApprovalDashboard1.click();
			Thread.sleep(2000);
			Approvalobj.AwaitingforApproval.click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("//a[text()= '" + x + "' ]")).click();
			Thread.sleep(3000);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("scroll(0,650)");
			Thread.sleep(3000);
			
			
			
			Approvalobj.Approvestatus.click();
			Approvalobj.statusdropdown.sendKeys("Approve");
			Approvalobj.statusdropdown.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
			Thread.sleep(3000);
			Approvalobj.Remarks.sendKeys("CaseApproved");
			Thread.sleep(3000);
			Approvalobj.submit.click();
			Thread.sleep(6000);
			Approvalobj.caseapproved.click();
			}	
			
			
			
			
			
			
			
		
		@Test(priority=9)
		public void managerlogout2() {
			
			Beneficiaryobjects.logouticon.click();
			Beneficiaryobjects.logoutbutton.click();
			
		}
		
		@Test(priority=10)
		public void managerlogin3() throws InterruptedException {
			Lobjects.Username.sendKeys("santoshcbu@immidartgroup.com");
			Lobjects.Password.sendKeys("password");
			Lobjects.signin.click();
			Thread.sleep(3000);
		}
		
		@Test(priority=11)
		public void manageraction3() throws InterruptedException {
			PageFactory.initElements(driver, Approvalobj);
			Thread.sleep(10000);
			//driver.findElement(By.xpath("//div[text()='Approval']")).click();
		Approvalobj.ApprovalDashboard.click();
		Thread.sleep(600);
		Approvalobj.AwaitingforApproval.click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//a[text()= '" + x + "' ]")).click();
		Thread.sleep(3000);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("scroll(0,650)");
		Thread.sleep(3000);
		
		
		
	
		
		Approvalobj.Approvestatus.click();
		Approvalobj.statusdropdown.sendKeys("Approve");
		Approvalobj.statusdropdown.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
		Thread.sleep(3000);
		Approvalobj.Remarks.sendKeys("CaseApproved");
		Thread.sleep(3000);
		Approvalobj.submit.click();
		Thread.sleep(6000);
		Approvalobj.caseapproved.click();
		}
		
		
		@Test(priority=12)
		public void managerlogout3() {
			
			Beneficiaryobjects.logouticon.click();
			Beneficiaryobjects.logoutbutton.click();
			
		}
		@Test(priority =13)

		public void Beneficiaryaction() throws InterruptedException {
			
			PageFactory.initElements(driver, Lobjects);
			driver.manage().timeouts().implicitlyWait(600, TimeUnit.SECONDS);
			//driver.get("http://10.10.10.101/enterpriselatest");
			Lobjects.Username.sendKeys("tl1@mailinator.com");
			Lobjects.Password.sendKeys("Password@1");
			Lobjects.signin.click();
			Thread.sleep(3000);
			PageFactory.initElements(driver, Beneficiaryobjects);
			Thread.sleep(10000);
			Beneficiaryobjects.DashboardMytasks.click();
			Thread.sleep(3000);
			Beneficiaryobjects.Mycases.click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("//small[text()= '" + x + "' ]")).click();
		    Thread.sleep(5000);
			//declaration
			driver.findElement(By.xpath("//button[contains(text(), 'Accept')]")).click();
			Thread.sleep(30000);

			Beneficiaryobjects.Questinnaire.click();
			Thread.sleep(2000);
			Beneficiaryobjects.Questinnairecompleted.click();
			Thread.sleep(2000);
			Beneficiaryobjects.Questionnairedialogbox.click();
			Thread.sleep(3000);
			 

			Beneficiaryobjects.Documenttab1.click();
			Thread.sleep(3000);
			Beneficiaryobjects.DocumentUploaded.click();
			Thread.sleep(3000);
			Beneficiaryobjects.Questionnairedialogbox.click();
			Thread.sleep(3000);
			Beneficiaryobjects.SubmitCase.click();
			Thread.sleep(3000);
		   
		    }
		
		
		@Test(priority = 14)
		public void Screeninglogin() throws InterruptedException {
			driver.manage().timeouts().implicitlyWait(600, TimeUnit.SECONDS);
			PageFactory.initElements(driver, Lobjects);
			//driver.get("http://10.10.10.101/enterpriselatest");
			Lobjects.Username.sendKeys("screen@mailinator.com");
			Lobjects.Password.sendKeys("password");
			Lobjects.signin.click();
			Thread.sleep(3000);
		}

		

		@Test(priority=15)
		public void screeningaction() throws InterruptedException {
			PageFactory.initElements(driver, Advancesearchobj);
			PageFactory.initElements(driver, Beneficiaryobjects);
			driver.manage().timeouts().implicitlyWait(600, TimeUnit.SECONDS);
			Advancesearchobj.countrySearchExpand.click();
			Thread.sleep(600);
			Advancesearchobj.SourceCountry.click();
			Advancesearchobj.SourceCountrySearch.sendKeys("India");
			Advancesearchobj.SourceCountrySearch.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
			Advancesearchobj.DestinationCountry.click();
			Advancesearchobj.DestinationCountrySearch.sendKeys("Germany");
			Advancesearchobj.DestinationCountrySearch.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
			Thread.sleep(300);
			Advancesearchobj.service.click();
			Advancesearchobj.serviceSearch.sendKeys("Long-Term Transfers");
			Advancesearchobj.serviceSearch.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
			Thread.sleep(700);
			Advancesearchobj.type.click();
			Advancesearchobj.typeSearch.sendKeys("Work Permit-D");
			Advancesearchobj.typeSearch.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
			
			
			//to scroll down
			JavascriptExecutor js = (JavascriptExecutor)driver;
			js.executeScript("window.scrollBy(0,450)", "");
			Advancesearchobj.SearchButton.click();
			
			
		    Thread.sleep(3000);
			
			
			
			
			
		//driver.findElement(By.xpath("//*[contains(text(),'Work Allocation')]")).click();
		    //Advancesearchobj.screeningmodule.isDisplayed();
		   // Thread.sleep(3000);
		    //Advancesearchobj.advancesearch.click();
		   // Thread.sleep(3000);
		   // driver.findElement(By.xpath("//a[@caseinitiationid='300063000000001054']")).click();
		    driver.findElement(By.xpath("//a[text()= '" + x + "' ]")).click();
		    Thread.sleep(3000);
			Advancesearchobj.QuestionnaireModule.click();
		   Thread.sleep(3000);
		  Beneficiaryobjects.Questinnairecompleted.click();
		   Thread.sleep(3000);
		    Advancesearchobj.Questdialogbox.click();
		    Thread.sleep(3000);
		   
		    Advancesearchobj.DocumentModule.click();
		    Thread.sleep(3000);
		    Advancesearchobj.DocumentVerification.click();
		    Thread.sleep(3000);
		    Advancesearchobj.CaseDecision.click();
		    Thread.sleep(3000);
		    Advancesearchobj.CaseVerfiSelectAction.click();
		    Thread.sleep(3000);
		    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys("screening completed");
		    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
		    Thread.sleep(3000);
		    Advancesearchobj.CaseVerifRemark.sendKeys("Screening completed");
		    Thread.sleep(3000);
		    Advancesearchobj.CaseVerifSave.click();
		    Thread.sleep(4000);
		    
		}
		@Test(priority = 16)
		public void Processinglogin() throws InterruptedException {
			driver.manage().timeouts().implicitlyWait(600, TimeUnit.SECONDS);
			PageFactory.initElements(driver, Lobjects);
			//driver.get("http://10.10.10.101/enterpriselatest");
			Lobjects.Username.sendKeys("processing@immidartgroup.com");
			Lobjects.Password.sendKeys("password");
			Lobjects.signin.click();
			Thread.sleep(3000);
		}

		

		@Test(priority=17)
		public void processingaction() throws InterruptedException {
			PageFactory.initElements(driver, Advancesearchobj);
			PageFactory.initElements(driver, Beneficiaryobjects);
			driver.manage().timeouts().implicitlyWait(600, TimeUnit.SECONDS);
			Advancesearchobj.countrySearchExpand.click();
			Thread.sleep(600);
			Advancesearchobj.SourceCountry.click();
			Advancesearchobj.SourceCountrySearch.sendKeys("India");
			Advancesearchobj.SourceCountrySearch.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
			Advancesearchobj.DestinationCountry.click();
			Advancesearchobj.DestinationCountrySearch.sendKeys("Germany");
			Advancesearchobj.DestinationCountrySearch.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
			Thread.sleep(300);
			Advancesearchobj.service.click();
			Advancesearchobj.serviceSearch.sendKeys("Long-Term Transfers");
			Advancesearchobj.serviceSearch.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
			Thread.sleep(700);
			Advancesearchobj.type.click();
			Advancesearchobj.typeSearch.sendKeys("Work Permit-D");
			Advancesearchobj.typeSearch.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
			
			
			//to scroll down
			JavascriptExecutor js = (JavascriptExecutor)driver;
			js.executeScript("window.scrollBy(0,450)", "");
			Advancesearchobj.SearchButton.click();
			
			
		    Thread.sleep(3000);
			
			
			
			
			
		//driver.findElement(By.xpath("//*[contains(text(),'Work Allocation')]")).click();
		    //Advancesearchobj.screeningmodule.isDisplayed();
		   // Thread.sleep(3000);
		    //Advancesearchobj.advancesearch.click();
		   // Thread.sleep(3000);
		   // driver.findElement(By.xpath("//a[@caseinitiationid='300063000000001054']")).click();
		    driver.findElement(By.xpath("//a[text()= '" + x + "' ]")).click();
		    Thread.sleep(3000);
			Advancesearchobj.QuestionnaireModule.click();
		   Thread.sleep(3000);
		  Beneficiaryobjects.Questinnairecompleted.click();
		   Thread.sleep(3000);
		    Advancesearchobj.Questdialogbox.click();
		    
		    Thread.sleep(3000);
		    Advancesearchobj.DocumentModule.click();
		    Thread.sleep(3000);
		    Advancesearchobj.DocumentVerification.click();
		    Thread.sleep(3000);
		    Advancesearchobj.CaseDecision.click();
		    Thread.sleep(3000);
		    Advancesearchobj.CaseVerfiSelectAction.click();
		    Thread.sleep(3000);
		    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys("Processing Acceptance");
		    Advancesearchobj.CaseVerfiSelectActionSearch.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);
		    Thread.sleep(3000);
		    Advancesearchobj.CaseVerifRemark.sendKeys("processing completed");
		    Thread.sleep(3000);
		    Advancesearchobj.CaseVerifSave.click();
		    Thread.sleep(4000);
		    
		}	    
	
}
